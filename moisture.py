from AWSIoTPythonSDK.MQTTLib import AWSIoTMQTTShadowClient
import random, time

from aliyunsdkcore import client
from aliyunsdkiot.request.v20180120 import RegisterDeviceRequest
from aliyunsdkiot.request.v20180120 import PubRequest, QueryProductTopicRequest, QueryTopicRouteTableRequest,  UpdateDeviceShadowRequest

# Steps performed for connecting the laptop to the AWS IoT Cloud
#
# 1. Create a secure policy (IAM policy) to determine the rights the moisture sensor will have
# 2. Register a thing - moisture sensor
# 3. Select one-click certificate creation and download the files referenced below via ROOT_CA, PRIVATE_KEY and CERT_FILE
# 4. Activate the thing and attach the above policy
# 5. Create a rule that will send an email whenever the sensor will publish 'moisture':'low' to a particular topic using SNS
# 6. Subscribe to that topic and approve it.
# 7. Test the setup with the following runAws() function.

# Amazon AWS variables
SHADOW_CLIENT = "acorneaMoistureClient"
HOST_NAME = "a9qffj4ha6njr-ats.iot.eu-central-1.amazonaws.com"
ROOT_CA = "AmazonRootCA1.pem"
PRIVATE_KEY = "c836878e95-private.pem.key"
CERT_FILE = "c836878e95-certificate.pem.crt"
SHADOW_HANDLER = "AndreeaCornea_MoistureSensor"

def awsUpdateConsoleCallback(payload, responseStatus, token):
    print('\n')
    print('Operation: $aws/things/AndreeaCornea_MoistureSensor/shadow/update')
    print("Payload = " + payload)
    print("ResponseStatus = " + responseStatus)
    print("Token = " + token)

def runAws():
    shadowClient = AWSIoTMQTTShadowClient(SHADOW_CLIENT)
    shadowClient.configureEndpoint(HOST_NAME, 8883)
    shadowClient.configureCredentials(ROOT_CA, PRIVATE_KEY, CERT_FILE)
    shadowClient.configureConnectDisconnectTimeout(10)
    shadowClient.configureMQTTOperationTimeout(5)
    shadowClient.connect()

    shadowDevice = shadowClient.createShadowHandlerWithName(SHADOW_HANDLER, True)

    while True:
        if random.choice([True, False]):
            msg = '{"state":{"reported":{"moisture":"high"}}}'
            shadowDevice.shadowUpdate(msg, awsUpdateConsoleCallback, 5)
        else:
            msg = '{"state":{"reported":{"moisture":"low"}}}'
            shadowDevice.shadowUpdate(msg, awsUpdateConsoleCallback, 5)

        time.sleep(10)


# Steps performed for connecting the laptop to the Alibaba IoT Cloud
#
# 1. Create a user and get the AccessKey and AccessKeySecret
# 2. Activate the IoT platform
# 3. Create a product and store the ProductKey
# 4. Associate the product with a device and store the DeviceName
# 5. Test the Alibaba IoT Platform using the runAliyun() function


#Aliyun variables
AliyunProductKey = 'a1UTo1ZBhA3'
AliyunDeviceName = "MoistureSensor"
UserAccessKey = "LTAI4FoSY8cZT7Hn1TVY98A8"
UserAccessKeySecret = "p1w3E8mKx3qm6VgVbyx0D21fVYgzUY"

def runAliyun():
    clt = client.AcsClient(UserAccessKey, UserAccessKeySecret, 'cn-shanghai')

    request = PubRequest.PubRequest()
    request.set_accept_format('json')
    request.set_ProductKey(AliyunProductKey)
    request.set_TopicFullName('/' + AliyunProductKey + '/' + AliyunDeviceName + '/user/update')
    request.set_MessageContent('YWNvcm5lYQ==') # Base64 for acornea
    request.set_Qos(1)

    result = clt.do_action_with_exception(request)
    print('Aliyun result: ' + result.decode())